package com.fengyunhe.helper;

import com.fengyunhe.helper.http.HttpClientHelper;
import org.apache.http.HttpResponse;

import java.io.File;
import java.io.IOException;

/**
 * Created by yangyan on 2015/7/13.
 */
public class TestHttpClientHelper {


    @org.junit.Test
    public void testGetFileName() {
        File file = null;
        try {
            file = File.createTempFile("111", "tmp");
        } catch (IOException e) {
            e.printStackTrace();
        }
        String url = "http://music.baidu.com/pc/spec/download9117/BaiduMusic-12345617.exe";
        try {
            HttpResponse httpResponse = HttpClientHelper.INSTANCE.get(url, null, null);
            assert "BaiduMusic-12345617.exe".equals(HttpClientHelper.INSTANCE.getFileName(httpResponse));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @org.junit.Test
    public void testDownloadFile() {
        File file = null;
        try {
            file = File.createTempFile("111", "tmp");
        } catch (IOException e) {
            e.printStackTrace();
        }
        String url = "http://music.baidu.com/pc/spec/download9117/BaiduMusic-12345617.exe";
        try {
            HttpClientHelper.INSTANCE.downloadFile(url, file);

            System.out.println(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

package com.fengyunhe.helper.sdk.wechatmp;

public enum MsgType {
    text, image, voice, video, location, link, event, news, music
}

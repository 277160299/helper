package com.fengyunhe.helper.sdk.wechatmp;

public enum MediaType {
    image, voice, video, thumb
}

package com.fengyunhe.helper.sdk.wechatmp.pay.util;

public class SDKRuntimeException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public SDKRuntimeException(String str) {
        super(str);
    }
}

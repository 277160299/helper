package com.fengyunhe.helper.sdk.wechatmp.send.msg;

//{
//    "touser":"OPENID",
//    "msgtype":"video",
//    "video":
//    {
//      "media_id":"MEDIA_ID",
//      "title":"TITLE",
//      "description":"DESCRIPTION"
//    }
//}

import com.fengyunhe.helper.sdk.wechatmp.api.bean.Video;

/**
 * 客服消息：视频
 *
 * @author Administrator
 */
public class VideoMsg extends AbstractSendMsg
{
    private Video video;


    public Video getVideo()
    {
        return video;
    }

    public void setVideo(Video video)
    {
        this.video = video;
    }


}

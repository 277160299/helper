package com.fengyunhe.helper.sdk.wechatmp;

import com.fengyunhe.helper.sdk.wechatmp.api.util.JsonObjectUtils;
import com.fengyunhe.helper.sdk.wechatmp.req.UnknownRequestMessage;
import com.fengyunhe.helper.sdk.wechatmp.req.AbstractRequestMessage;
import com.fengyunhe.helper.sdk.wechatmp.req.msg.*;

public class DefaultMessageHandlerImpl implements MessageHandler {
    // Log log = LogFactory.getLog(this.getClass());
    private com.fengyunhe.helper.sdk.wechatmp.resp.msg.TextSyncMessage ok(AbstractRequestMessage message) {
        com.fengyunhe.helper.sdk.wechatmp.resp.msg.TextSyncMessage textSyncMessage = new com.fengyunhe.helper.sdk.wechatmp.resp.msg.TextSyncMessage();
        textSyncMessage.setContent(JsonObjectUtils.beanToJson(message));
        textSyncMessage.setCreateTime(System.currentTimeMillis());
        textSyncMessage.setFromUserName(message.getToUserName());
        textSyncMessage.setToUserName(message.getFromUserName());
        return textSyncMessage;
    }

    @Override
    public com.fengyunhe.helper.sdk.wechatmp.resp.msg.AbstractRespMessage handleVoice(VoiceMessage message) {
        return ok(message);
    }

    @Override
    public com.fengyunhe.helper.sdk.wechatmp.resp.msg.AbstractRespMessage handleVideo(VideoMessage message) {
        return ok(message);
    }

    @Override
    public com.fengyunhe.helper.sdk.wechatmp.resp.msg.AbstractRespMessage handleText(TextMessage message) {
        return ok(message);
    }

    @Override
    public com.fengyunhe.helper.sdk.wechatmp.resp.msg.AbstractRespMessage handleLocation(LocationMessage message) {
        return ok(message);
    }

    @Override
    public com.fengyunhe.helper.sdk.wechatmp.resp.msg.AbstractRespMessage handleLink(LinkMessage message) {
        return ok(message);
    }

    @Override
    public com.fengyunhe.helper.sdk.wechatmp.resp.msg.AbstractRespMessage handleImage(ImageMessage message) {
        return ok(message);
    }

    @Override
    public com.fengyunhe.helper.sdk.wechatmp.resp.msg.AbstractRespMessage handleUnknownMessage(
            UnknownRequestMessage message) {
        return ok(message);
    }
}

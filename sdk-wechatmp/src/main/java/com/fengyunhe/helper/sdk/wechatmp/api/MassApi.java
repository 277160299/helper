package com.fengyunhe.helper.sdk.wechatmp.api;

import com.fengyunhe.helper.sdk.wechatmp.api.bean.Article;
import com.fengyunhe.helper.sdk.wechatmp.api.bean.MediaInfo;

import java.util.List;

/**
 * 功能：群发API，群发接口成功后将会返回msgId，否则将抛出异常
 * 作者： yangyan
 * 时间： 2014/8/16 .
 */
public interface MassApi {

    /**
     * 上传图文素材
     *
     * @param accessToken
     * @param articleList
     * @return
     */
    MediaInfo uploadNews(String accessToken, List<Article> articleList);

    /**
     * 获取视频素材mediaId
     *
     * @param accessToken
     * @param mediaId
     * @param title
     * @param description
     * @return
     */
    MediaInfo getUploadVideoMediaId(String accessToken, String mediaId, String title, String description);

    /**
     * 群发图文到组
     *
     * @param accessToken
     * @param groupId
     * @param mediaId
     * @return
     */
    public String sendNewsToGroup(String accessToken, String mediaId, String groupId);

    /**
     * 群发图文到多个用户
     *
     * @param accessToken
     * @param mediaId
     * @param openId
     * @return
     */
    public String sendNewsToUsers(String accessToken, String mediaId, String... openId);

    /**
     * 群发文本到组
     *
     * @param accessToken
     * @param content
     * @param groupId
     * @return
     */
    public String sendTextToGroup(String accessToken, String content, String groupId);

    /**
     * 群发文本到多个用户
     *
     * @param accessToken
     * @param content
     * @param openId
     * @return
     */
    public String sendTextToUsers(String accessToken, String content, String... openId);

    /**
     * 群发声音到组
     *
     * @param accessToken
     * @param mediaId
     * @param groupId
     * @return
     */
    public String sendVoiceToGroup(String accessToken, String mediaId, String groupId);

    /**
     * 群发语音到多个用户
     *
     * @param accessToken
     * @param mediaId
     * @param openId
     * @return
     */
    public String sendVoiceToUsers(String accessToken, String mediaId, String... openId);

    /**
     * 群发图片到组
     *
     * @param accessToken
     * @param mediaId
     * @param groupId
     * @return
     */
    public String sendImageToGroup(String accessToken, String mediaId, String groupId);


    /**
     * 群发图片到多个用户
     *
     * @param accessToken
     * @param mediaId
     * @param openId
     * @return
     */
    public String sendImageToUsers(String accessToken, String mediaId, String... openId);

    /**
     * 群发视频到组
     *
     * @param accessToken
     * @param mediaId
     * @param title
     * @param description
     * @param groupId     @return
     */
    public String sendVideoToGroup(String accessToken, String mediaId, String title, String description, String groupId);


    /**
     * 群发视频到多个用户
     *
     * @param accessToken
     * @param mediaId
     * @param title
     * @param descritpion
     * @param openId      @return
     */
    public String sendVideoToUsers(String accessToken, String mediaId, String title, String descritpion, String... openId);


    /**
     * 删除群发消息（图文或视频，其他无法删除）
     *
     * @param accessToken
     * @param msgId
     * @return
     */
    public boolean delete(String accessToken, String msgId);

}

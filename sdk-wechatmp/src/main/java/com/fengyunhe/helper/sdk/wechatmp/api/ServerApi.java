package com.fengyunhe.helper.sdk.wechatmp.api;

import com.fengyunhe.helper.sdk.wechatmp.MediaType;
import com.fengyunhe.helper.sdk.wechatmp.api.bean.Attachment;
import com.fengyunhe.helper.sdk.wechatmp.api.bean.MediaInfo;
import com.fengyunhe.helper.sdk.wechatmp.api.bean.ServerAccessToken;

import java.io.File;

/**
 * 通用基础API
 */
public interface ServerApi {

    /**
     * 返回公众号accessToken
     *
     * @return
     */
    ServerAccessToken getAccessToken();

    /**
     * 下载媒体文件
     *
     * @param accessToken
     * @param mediaId
     * @return
     */
    Attachment getMedia(String accessToken, String mediaId);

    /**
     * 上传媒体文件
     *
     * @param accessToken
     * @param type
     * @param file
     * @return
     */
    MediaInfo uploadMedia(String accessToken, MediaType type, File file);


    /**
     * 上传图片到微信服务器
     *
     * @param accessToken
     * @param file
     * @return 返回微信服务器的图片url
     */
    String uploadImg(String accessToken, File file);


    /**
     * 获取临时二维码兑换码
     *
     * @param accessToken
     * @param sceneId
     * @return
     */
    String getTempQrcodeTicket(String accessToken, long sceneId);

    /**
     * 获取二维码兑换码
     *
     * @param accessToken
     * @param sceneId     场景id,数字1-100000
     * @return
     */
    String getQrcodeTicket(String accessToken, int sceneId);

    /**
     * 获取二维码兑换码
     *
     * @param accessToken
     * @param sceneStr    场景字符串,字符串1-64长度
     * @return
     */
    String getQrcodeTicket(String accessToken, String sceneStr);

    /**
     * 获取二维码
     *
     * @param ticket
     * @return
     */
    String getQrcodeUrl(String ticket);

    /**
     * 长连接转短连接
     *
     * @param accessToken
     * @param longUrl
     * @return
     */
    String getShortUrl(String accessToken, String longUrl);
}

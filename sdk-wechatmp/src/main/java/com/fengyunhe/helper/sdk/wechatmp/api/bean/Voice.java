package com.fengyunhe.helper.sdk.wechatmp.api.bean;

public class Voice {

    private String media_id;

    public final String getMedia_id() {
        return media_id;
    }

    public final void setMedia_id(String media_id) {
        this.media_id = media_id;
    }

}

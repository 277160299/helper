package com.fengyunhe.helper.sdk.wechatmp.api.impl;

import com.fengyunhe.helper.sdk.wechatmp.WeChatApp;
import com.fengyunhe.helper.sdk.wechatmp.api.ShopApi;

/**
 * 微信小店API
 * Created by yangyan on 2015/6/1.
 */
public class ShopApiImpl implements ShopApi {

    private final WeChatApp app;

    public ShopApiImpl(WeChatApp app) {
        this.app = app;
    }
}

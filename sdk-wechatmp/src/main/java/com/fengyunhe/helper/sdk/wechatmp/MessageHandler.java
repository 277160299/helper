package com.fengyunhe.helper.sdk.wechatmp;

import com.fengyunhe.helper.sdk.wechatmp.req.UnknownRequestMessage;
import com.fengyunhe.helper.sdk.wechatmp.req.msg.*;

/**
 * 消息处理
 *
 * @author yangyan
 */
public interface MessageHandler {

    /**
     * 响应文本消息
     *
     * @param message
     * @return
     */
    public com.fengyunhe.helper.sdk.wechatmp.resp.msg.AbstractRespMessage handleText(TextMessage message);

    /**
     * 响应图片消息
     *
     * @param message
     * @return
     */
    public com.fengyunhe.helper.sdk.wechatmp.resp.msg.AbstractRespMessage handleImage(ImageMessage message);

    /**
     * 响应链接消息
     *
     * @param message
     * @return
     */
    public com.fengyunhe.helper.sdk.wechatmp.resp.msg.AbstractRespMessage handleLink(LinkMessage message);

    /**
     * 响应位置消息
     *
     * @param message
     * @return
     */
    public com.fengyunhe.helper.sdk.wechatmp.resp.msg.AbstractRespMessage handleLocation(LocationMessage message);

    /**
     * 响应视频消息
     *
     * @param message
     * @return
     */
    public com.fengyunhe.helper.sdk.wechatmp.resp.msg.AbstractRespMessage handleVideo(VideoMessage message);

    /**
     * 响应语音消息
     *
     * @param message
     * @return
     */
    public com.fengyunhe.helper.sdk.wechatmp.resp.msg.AbstractRespMessage handleVoice(VoiceMessage message);

    /**
     * 响应暂时无法识别的消息类型
     *
     * @param message
     * @return
     */
    public com.fengyunhe.helper.sdk.wechatmp.resp.msg.AbstractRespMessage handleUnknownMessage(
            UnknownRequestMessage message);

}

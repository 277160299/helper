package com.fengyunhe.helper.sdk.wechatmp.resp.msg;

import com.fengyunhe.helper.sdk.wechatmp.api.util.MessageUtil;
import com.fengyunhe.helper.sdk.wechatmp.msg.SyncMessage;

public abstract class AbstractRespMessage extends SyncMessage {
    @Override
    public String toString() {
        return MessageUtil.respMessageToXml(this);
    }
}

package com.fengyunhe.helper.sdk.wechatmp.pay;

import com.fengyunhe.helper.sdk.wechatmp.pay.bean.WarningNotify;

/**
 * 默认微信支付消息事件实现类
 *
 * @author Administrator
 */
public class DefaultPayEventHandlerImpl implements PayEventHandler {

    @Override
    public boolean onWarningNotify(WarningNotify warningNotify) {
        return false;
    }

}

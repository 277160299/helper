package com.fengyunhe.helper.sdk.wechatmp;

import com.fengyunhe.helper.sdk.wechatmp.req.AbstractRequestMessage;
import com.fengyunhe.helper.sdk.wechatmp.req.event.*;

public class DefaultEventHandlerImpl implements EventHandler
{


    private com.fengyunhe.helper.sdk.wechatmp.resp.msg.TextSyncMessage ok(AbstractRequestMessage message)
    {
//        TextSyncMessage textSyncMessage = new TextSyncMessage();
//        textSyncMessage.setContent(message.getMsgType());
//        textSyncMessage.setCreateTime(System.currentTimeMillis());
//        textSyncMessage.setFromUserName(message.getToUserName());
//        textSyncMessage.setToUserName(message.getFromUserName());
//        textSyncMessage.setContent(JsonObjectUtils.beanToJson(message));
//        return textSyncMessage;
        return null;
    }

    @Override
    public com.fengyunhe.helper.sdk.wechatmp.resp.msg.AbstractRespMessage handleUnsubscribe(UnsubscribeEvent event)
    {
        return ok(event);
    }

    @Override
    public com.fengyunhe.helper.sdk.wechatmp.resp.msg.AbstractRespMessage handleSubscribe(SubscribeEvent event)
    {

        return ok(event);
    }

    @Override
    public com.fengyunhe.helper.sdk.wechatmp.resp.msg.AbstractRespMessage handleScanSubscribe(ScanSubscribeEvent event)
    {
        return ok(event);
    }

    @Override
    public com.fengyunhe.helper.sdk.wechatmp.resp.msg.AbstractRespMessage handleScan(ScanEvent event)
    {
        return ok(event);
    }

    @Override
    public com.fengyunhe.helper.sdk.wechatmp.resp.msg.AbstractRespMessage handleMenuClick(MenuClickEvent event)
    {
        return ok(event);
    }

    @Override
    public com.fengyunhe.helper.sdk.wechatmp.resp.msg.AbstractRespMessage handleLocation(LocationEvent event)
    {
        return ok(event);
    }

    @Override
    public com.fengyunhe.helper.sdk.wechatmp.resp.msg.AbstractRespMessage handleMenuViewClick(MenuViewClickEvent event)
    {
        return ok(event);
    }

    @Override
    public com.fengyunhe.helper.sdk.wechatmp.resp.msg.AbstractRespMessage handleScanCodePushEvent(ScanCodePushEvent event)
    {
        return ok(event);
    }

    @Override
    public com.fengyunhe.helper.sdk.wechatmp.resp.msg.AbstractRespMessage handleScanCodeWaitMsgEvent(ScanCodeWaitMsgEvent event)
    {
        return ok(event);
    }

    @Override
    public com.fengyunhe.helper.sdk.wechatmp.resp.msg.AbstractRespMessage handlePicSysphoneEvent(PicSysphoneEvent event)
    {
        return ok(event);
    }

    @Override
    public com.fengyunhe.helper.sdk.wechatmp.resp.msg.AbstractRespMessage handlePicPhotoOrAlbumEvent(PicPhotoOrAlbumEvent event)
    {
        return ok(event);
    }

    @Override
    public com.fengyunhe.helper.sdk.wechatmp.resp.msg.AbstractRespMessage handlePicWeixinEvent(PicWeixinEvent event)
    {
        return ok(event);
    }

    @Override
    public com.fengyunhe.helper.sdk.wechatmp.resp.msg.AbstractRespMessage handleLocationSelectEvent(LocationSelectEvent event)
    {
        return ok(event);
    }

    @Override
    public void handleMassSendJobFinsh(MassSendJobFinshEvent event)
    {

    }
}

package com.fengyunhe.helper.sdk.wechatmp.req;

public class UnknownRequestMessage extends AbstractRequestMessage {
    private String message = null;

    public UnknownRequestMessage(String message) {
        this.message = message;
    }
}

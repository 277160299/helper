package com.fengyunhe.helper.sdk.wechatmp.api.impl;

import com.fengyunhe.helper.sdk.wechatmp.ErrorCode;
import com.fengyunhe.helper.sdk.wechatmp.api.MassApi;
import com.fengyunhe.helper.sdk.wechatmp.api.bean.Article;
import com.fengyunhe.helper.sdk.wechatmp.api.bean.MediaInfo;
import com.fengyunhe.helper.sdk.wechatmp.api.util.HttpClientHelper;
import com.fengyunhe.helper.sdk.wechatmp.api.util.JsonObjectUtils;
import org.codehaus.jackson.JsonNode;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 功能：群发消息
 * 作者： yangyan
 * 时间： 2014/8/16 .
 */
public class MassApiImpl implements MassApi {
    final static String SEND_TO_GROUP_URL = "https://api.weixin.qq.com/cgi-bin/message/mass/sendall?access_token=";
    final static String SEND_TO_USERS_URL = "https://api.weixin.qq.com/cgi-bin/message/mass/send?access_token=";
    final static String DELETE_URL = "https://api.weixin.qq.com//cgi-bin/message/mass/delete?access_token=";


    @Override
    public MediaInfo uploadNews(String accessToken, List<Article> articleList) {
        String url = "https://api.weixin.qq.com/cgi-bin/media/uploadnews?access_token=" + accessToken;
        Map<String, Object> params = new java.util.HashMap<String, Object>();
        params.put("articles", articleList);
        String jsonStr = null;
        try {
            jsonStr = HttpClientHelper.INSTANCE.post(url, JsonObjectUtils.beanToJson(params));
        } catch (IOException e) {
            e.printStackTrace();
        }
        JsonNode jsonNode = JsonObjectUtils.stringToJsonNode(jsonStr);
        ErrorCode.check(jsonNode);
        return JsonObjectUtils.jsonToBean(jsonStr, MediaInfo.class);

    }

    @Override
    public MediaInfo getUploadVideoMediaId(String accessToken, String mediaId, String title, String description) {
        String url = "https://file.api.weixin.qq.com/cgi-bin/media/uploadvideo?access_token=" + accessToken;
        Map<String, Object> params = new java.util.HashMap<String, Object>();
        params.put("media_id", mediaId);
        params.put("title", title);
        params.put("description", description);

        String jsonStr = null;
        try {
            jsonStr = HttpClientHelper.INSTANCE.post(url, JsonObjectUtils.beanToJson(params));
        } catch (IOException e) {
            e.printStackTrace();
        }
        JsonNode jsonNode = JsonObjectUtils.stringToJsonNode(jsonStr);
        ErrorCode.check(jsonNode);
        return JsonObjectUtils.jsonToBean(jsonStr, MediaInfo.class);
    }

    @Override
    public String sendNewsToGroup(String accessToken, String mediaId, String groupId) {

//        {
//            "filter":{
//              "group_id":"2"
//            },
//            "mpnews":{
//                "media_id":"123dsdajkasd231jhksad"
//            },
//            "msgtype":"mpnews"
//        }

        Map<String, Object> json = new HashMap<String, Object>();
        json.put("msgtype", "mpnews");
        Map<String, Object> mpnews = new HashMap<String, Object>();
        mpnews.put("media_id", mediaId);
        json.put("mpnews", mpnews);
        Map<String, Object> filter = new HashMap<String, Object>();
        filter.put("group_id", groupId);
        json.put("filter", filter);
        String post = JsonObjectUtils.beanToJson(json);
        //System.out.println(post);
        String result = null;
        try {
            result = HttpClientHelper.INSTANCE.post(SEND_TO_GROUP_URL.concat(accessToken), post);
        } catch (IOException e) {
            e.printStackTrace();
        }
        JsonNode check = ErrorCode.check(result);
        return check.get("msg_id").getTextValue();
    }

    @Override
    public String sendNewsToUsers(String accessToken, String mediaId, String... openId) {
        Map<String, Object> json = new HashMap<String, Object>();
        json.put("msgtype", "mpnews");
        Map<String, Object> mpnews = new HashMap<String, Object>();
        mpnews.put("media_id", mediaId);
        json.put("mpnews", mpnews);
        Map<String, Object> filter = new HashMap<String, Object>();
        filter.put("touser", openId);
        json.put("filter", filter);
        String post = JsonObjectUtils.beanToJson(json);
        //System.out.println(post);
        String result = null;
        try {
            result = HttpClientHelper.INSTANCE.post(SEND_TO_USERS_URL.concat(accessToken), post);
        } catch (IOException e) {
            e.printStackTrace();
        }
        JsonNode check = ErrorCode.check(result);
        return check.get("msg_id").getTextValue();
    }

    @Override
    public String sendTextToGroup(String accessToken, String content, String groupId) {
        Map<String, Object> json = new HashMap<String, Object>();
        json.put("msgtype", "text");
        Map<String, Object> text = new HashMap<String, Object>();
        text.put("content", content);
        json.put("text", text);
        Map<String, Object> filter = new HashMap<String, Object>();
        filter.put("group_id", groupId);
        json.put("filter", filter);
        String post = JsonObjectUtils.beanToJson(json);
        //System.out.println(post);
        String result = null;
        try {
            result = HttpClientHelper.INSTANCE.post(SEND_TO_GROUP_URL.concat(accessToken), post);
        } catch (IOException e) {
            e.printStackTrace();
        }
        JsonNode check = ErrorCode.check(result);
        return check.get("msg_id").getTextValue();
    }

    @Override
    public String sendTextToUsers(String accessToken, String content, String... openId) {
        Map<String, Object> json = new HashMap<String, Object>();
        json.put("msgtype", "text");
        Map<String, Object> text = new HashMap<String, Object>();
        text.put("content", content);
        json.put("text", text);
        Map<String, Object> filter = new HashMap<String, Object>();
        filter.put("touser", openId);
        json.put("filter", filter);
        String post = JsonObjectUtils.beanToJson(json);
        //System.out.println(post);
        String result = null;
        try {
            result = HttpClientHelper.INSTANCE.post(SEND_TO_USERS_URL.concat(accessToken), post);
        } catch (IOException e) {
            e.printStackTrace();
        }
        JsonNode check = ErrorCode.check(result);
        return check.get("msg_id").getTextValue();
    }

    @Override
    public String sendVoiceToGroup(String accessToken, String mediaId, String groupId) {
        Map<String, Object> json = new HashMap<String, Object>();
        json.put("msgtype", "voice");
        Map<String, Object> voice = new HashMap<String, Object>();
        voice.put("media_id", mediaId);
        json.put("voice", voice);
        Map<String, Object> filter = new HashMap<String, Object>();
        filter.put("group_id", groupId);
        json.put("filter", filter);
        String post = JsonObjectUtils.beanToJson(json);
        //System.out.println(post);
        String result = null;
        try {
            result = HttpClientHelper.INSTANCE.post(SEND_TO_GROUP_URL.concat(accessToken), post);
        } catch (IOException e) {
            e.printStackTrace();
        }
        JsonNode check = ErrorCode.check(result);
        return check.get("msg_id").getTextValue();
    }

    @Override
    public String sendVoiceToUsers(String accessToken, String mediaId, String... openId) {
        Map<String, Object> json = new HashMap<String, Object>();
        json.put("msgtype", "voice");
        Map<String, Object> voice = new HashMap<String, Object>();
        voice.put("media_id", mediaId);
        json.put("voice", voice);
        Map<String, Object> filter = new HashMap<String, Object>();
        filter.put("touser", openId);
        json.put("filter", filter);
        String post = JsonObjectUtils.beanToJson(json);
        //System.out.println(post);
        String result = null;
        try {
            result = HttpClientHelper.INSTANCE.post(SEND_TO_USERS_URL.concat(accessToken), post);
        } catch (IOException e) {
            e.printStackTrace();
        }
        JsonNode check = ErrorCode.check(result);
        return check.get("msg_id").getTextValue();
    }

    @Override
    public String sendImageToGroup(String accessToken, String mediaId, String groupId) {
        Map<String, Object> json = new HashMap<String, Object>();
        json.put("msgtype", "image");
        Map<String, Object> image = new HashMap<String, Object>();
        image.put("media_id", mediaId);
        json.put("image", image);
        Map<String, Object> filter = new HashMap<String, Object>();
        filter.put("group_id", groupId);
        json.put("filter", filter);
        String post = JsonObjectUtils.beanToJson(json);
        //System.out.println(post);
        String result = null;
        try {
            result = HttpClientHelper.INSTANCE.post(SEND_TO_GROUP_URL.concat(accessToken), post);
        } catch (IOException e) {
            e.printStackTrace();
        }
        JsonNode check = ErrorCode.check(result);
        return check.get("msg_id").getTextValue();
    }

    @Override
    public String sendImageToUsers(String accessToken, String mediaId, String... openId) {
        Map<String, Object> json = new HashMap<String, Object>();
        json.put("msgtype", "image");
        Map<String, Object> image = new HashMap<String, Object>();
        image.put("media_id", mediaId);
        json.put("image", image);
        Map<String, Object> filter = new HashMap<String, Object>();
        filter.put("touser", openId);
        json.put("filter", filter);
        String post = JsonObjectUtils.beanToJson(json);
        //System.out.println(post);
        String result = null;
        try {
            result = HttpClientHelper.INSTANCE.post(SEND_TO_USERS_URL.concat(accessToken), post);
        } catch (IOException e) {
            e.printStackTrace();
        }
        JsonNode check = ErrorCode.check(result);
        return check.get("msg_id").getTextValue();
    }

    @Override
    public String sendVideoToGroup(String accessToken, String mediaId, String title, String description, String groupId) {
        Map<String, Object> json = new HashMap<String, Object>();
        json.put("msgtype", "video");
        Map<String, Object> video = new HashMap<String, Object>();
        video.put("media_id", mediaId);
        video.put("title", title);
        video.put("description", description);
        json.put("video", video);
        Map<String, Object> filter = new HashMap<String, Object>();
        filter.put("group_id", groupId);
        json.put("filter", filter);
        String post = JsonObjectUtils.beanToJson(json);
        //System.out.println(post);
        String result = null;
        try {
            result = HttpClientHelper.INSTANCE.post(SEND_TO_GROUP_URL.concat(accessToken), post);
        } catch (IOException e) {
            e.printStackTrace();
        }
        JsonNode check = ErrorCode.check(result);
        return check.get("msg_id").getTextValue();
    }

    @Override
    public String sendVideoToUsers(String accessToken, String mediaId, String title, String description, String... openId) {
        Map<String, Object> json = new HashMap<String, Object>();
        json.put("msgtype", "video");
        Map<String, Object> video = new HashMap<String, Object>();
        video.put("media_id", mediaId);
        video.put("title", title);
        video.put("description", description);
        json.put("video", video);
        Map<String, Object> filter = new HashMap<String, Object>();
        filter.put("touser", openId);
        json.put("filter", filter);
        String post = JsonObjectUtils.beanToJson(json);
        //System.out.println(post);
        String result = null;
        try {
            result = HttpClientHelper.INSTANCE.post(SEND_TO_USERS_URL.concat(accessToken), post);
        } catch (IOException e) {
            e.printStackTrace();
        }
        JsonNode check = ErrorCode.check(result);
        return check.get("msg_id").getTextValue();
    }

    @Override
    public boolean delete(String accessToken, String msgId) {
        Map<String, Object> json = new HashMap<String, Object>();
        json.put("msg_id", "msgId");
        String post = JsonObjectUtils.beanToJson(json);
        String result = null;
        try {
            result = HttpClientHelper.INSTANCE.post(DELETE_URL.concat(accessToken), post);
        } catch (IOException e) {
            e.printStackTrace();
        }
        JsonNode check = ErrorCode.check(result);
        return true;
    }
}

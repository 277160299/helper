package com.fengyunhe.helper.sdk.wechatmp.pay;

import com.fengyunhe.helper.sdk.wechatmp.pay.bean.WarningNotify;

/**
 * 微信支付消息事件
 *
 * @author Administrator
 */
public interface PayEventHandler {
    /**
     * 当警告通知
     *
     * @param warningNotify
     * @return
     */
    public boolean onWarningNotify(WarningNotify warningNotify);

}

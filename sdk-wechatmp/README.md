# 基于HttpClient4 实现的微信SDK

## 已经实现的的接口

* GroupApi 用户组接口
* JsApi JS签名接口
* MassApi 群发接口
* MenuApi 自定义菜单接口
* MessageApi 客服消息和响应消息接口,支持消息加密解密方式
* PayApi 微信红包接口,微信分裂红包接口
* ServerApi 公用接口
* UserApi 用户接口

## 待实现和完善的接口

* CardApi 卡卷接口：等待
* PayApi 微信支付接口：等待（v3版）
* PoiApi 门店接口：等待
* ShopApi 微信小店接口


## 使用方式

### 接口调用方法
````
    WeChatApp app = new WechatApp();
    app.setXXX();
    ...

    WeChat wechat = new Wechat(app);


    // 获取token
    wechat.getServerApi().getAccessToken();

    // 获取授权地址

    wechat.getUserApi().getAuthorizationUserInfoUrl('http://baidu.com','state');

    其他接口，一般需要传入accessToken，则可以直接用
    wechat.getServerApi().getAccessToken().getToken()获取到字符串，内部会在快过期的时候自动获取新的token。

````

### 接受消息和事件并处理 (Spring MVC)

````
@RequestMapping(method = RequestMethod.GET, value = "/api")
    public void get(HttpServletRequest request, HttpServletResponse response) {

        if (weChatApp == null || weChat == null) {
            return ;
        }
        String path = request.getServletPath();
        String pathInfo = path.substring(path.lastIndexOf("/"));
        String outPut = "";
        if (pathInfo != null) {
            String echostr = request.getParameter("echostr");//
            // 验证
            if (echostr != null
                    && Tools.checkSignature(
                    weChatApp.getToken(), request
                            .getParameter("signature"), request
                            .getParameter("timestamp"), request
                            .getParameter("nonce"))) {
                outPut = echostr;
            }
        }
        try {
            response.getWriter().write(outPut);
            response.getWriter().flush();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @RequestMapping(method = RequestMethod.POST, value = "/api")
    public void post(HttpServletRequest request, HttpServletResponse response) {

        if (weChatApp == null || weChat == null) {
            return ;
        }
        if (!Tools.checkSignature(weChatApp.getToken(), request.getParameter("signature"), request
                .getParameter("timestamp"), request.getParameter("nonce"))) {
            try {
                response.getWriter().print("");
                response.getWriter().flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return;
        }

        // 将请求、响应的编码均设置为UTF-8（防止中文乱码）
        try {
            request.setCharacterEncoding("UTF-8");
            response.setCharacterEncoding("UTF-8");
            response.setContentType("text/xml");
            String xml = weChat
                    .processing(request);
            response.getWriter().write(xml == null ? "" : xml);
            log.debug("响应给微信的XML：" + xml);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
````





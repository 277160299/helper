package com.fengyunhe.helper.hibernate3;


import org.apache.commons.lang3.StringUtils;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.engine.SessionFactoryImplementor;
import org.hibernate.engine.query.HQLQueryPlan;
import org.hibernate.engine.query.QueryPlanCache;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Hibernate dao 基础类，一个Hibernate的dao类继承此抽象类后，需要实现 setSessionFactory 方法，注入数据源
 *
 * @param <T>  对象类型
 * @param <PK> 对象主键类型
 */
public abstract class HibernateBaseDaoImpl<T, PK extends Serializable> implements
        HibernateDao<T, PK> {


    protected SessionFactory sessionFactory;
    private Class<T> entityClass;

    protected HibernateBaseDaoImpl() {

        Class c = getClass();
        Type type = c.getGenericSuperclass();
        if (type instanceof ParameterizedType) {
            Type[] parameterizedType = ((ParameterizedType) type)
                    .getActualTypeArguments();
            this.entityClass = (Class<T>) parameterizedType[0];
        }
    }

    /**
     * 注入对应的数据源
     *
     * @param sessionFactory
     */
    protected abstract void setSessionFactory(SessionFactory sessionFactory);

    @Override
    public Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public Session openSession() {
        return sessionFactory.openSession();
    }

    @Override
    public void beginTransaction() {
        getCurrentSession().beginTransaction();
    }

    @Override
    public void commitTransaction() {
        getCurrentSession().getTransaction().commit();
    }

    @Override
    public void rollbackTransaction() {
        getCurrentSession().getTransaction().rollback();
    }

    @Override
    public void closeSession(Session session) {
        session.close();
    }

    @Override
    public T getById(PK id) {
        return (T) getCurrentSession().get(entityClass, id);
    }

    @Override
    public T loadById(PK id) {
        return (T) getCurrentSession().load(entityClass, id);
    }

    @Override
    public <T1> T1 getPropertyById(PK id, String propertyName, Class<T1> clazz) {
        return getUniqueResult(clazz, "select " + propertyName + " from " + entityClass.getName() + " where " + sessionFactory.getClassMetadata(this.entityClass).getIdentifierPropertyName() + "=?", id);
    }

    @Override
    public void save(T model) {
        getCurrentSession().save(model);
    }

    @Override
    public void saveOrUpdate(T model) {
        getCurrentSession().saveOrUpdate(model);
    }

    @Override
    public List<T> findListByProperty(String propertyName, Object value) {
        Query query = getCurrentSession().createQuery("from " + entityClass.getName() + " where " + propertyName + "=?");
        query.setParameter(0, value);
        return query.list();
    }

    @Override
    public <T> List<T> findListByProperty(Class<T> resultClass, String getPropertyName, String propertyName, Object value) {
        Query query = getCurrentSession().createQuery("select " + getPropertyName + " from " + entityClass.getName() + " where " + propertyName + "=?");
        query.setParameter(0, value);
        return query.list();
    }

    @Override
    public void delete(T model) {
        getCurrentSession().delete(model);
    }

    @Override
    public void deleteById(PK... id) {
        for (int i = 0; i < id.length; i++) {
            execute("delete from " + entityClass.getName() + " where " + sessionFactory.getClassMetadata(this.entityClass).getIdentifierPropertyName() + "=?", id[i]);
        }
    }

    @Override
    public void update(T model) {
        getCurrentSession().update(model);
    }

    @Override
    public List<T> findAll() {
        return getCurrentSession().createCriteria(entityClass.getName()).list();
    }

    @Override
    public void deleteResultsByProperty(String propertyName, Object value) {
        Query query = getCurrentSession().createQuery("delete from " + entityClass.getName() + " where " + propertyName + "=?");
        query.setParameter(0, value);
        query.executeUpdate();
    }

    @Override
    public void deleteResultsByPropertyInValues(String propertyName, Object... value) {
        String s = StringUtils.leftPad("?", value.length * 2 - 1, "?,");
        Query query = getCurrentSession().createQuery("delete from " + entityClass.getName() + " where " + propertyName + " in (" + s + ") ");
        for (int i = 0; i < value.length; i++) {
            query.setParameter(i, value[i]);
        }
        query.executeUpdate();
    }

    @Override
    public List<T> findByPage(int page, int pageSize, String hql, List<Object> params) {
        Query q = getCurrentSession().createQuery(hql);
        q.setFirstResult((page - 1) * pageSize)
                .setMaxResults(pageSize);
        if (params != null) {
            for (int i = 0; i < params.size(); i++) {
                q.setParameter(i, params.get(i));
            }
        }
        return q.list();
    }

    @Override
    public List<T> findByPage(int page, int pageSize) {
        return this.findByPage(page, pageSize, "from " + this.entityClass.getName(),
                Collections.EMPTY_LIST);
    }

    @Override
    public List<T> findByPage(int page, int pageSize, String hql, Object... params) {
        return this.findByPage(page, pageSize, hql, Arrays.asList(params));
    }

    @Override
    public void clear() {
        getCurrentSession().clear();
    }

    @Override
    public void flush() {
        getCurrentSession().flush();
    }

    @Override
    public void evict(Object o) {
        getCurrentSession().evict(o);
    }

    @Override
    public boolean isExist(String propertyName, Object value) {
        String hql = "select count(*) from " + entityClass.getName()
                + " where " + propertyName + "=?";
        return (Long) getCurrentSession().createQuery(hql)
                .setParameter(0, value).uniqueResult() > 0;
    }

    @Override
    public long getTotalCount() {
        String hql = "select count(*) from " + entityClass.getName();
        return Long.valueOf(getCurrentSession().createQuery(hql).uniqueResult()
                .toString());
    }

    @Override
    public long getTotalCount(String hql, List<Object> params) {
        Query q = getCurrentSession().createQuery(prepareCountHql(hql));
        if (params != null) {
            for (int i = 0; i < params.size(); i++) {
                q.setParameter(i, params.get(i));
            }
        }
        Object singleResult = q.uniqueResult();
        if (singleResult instanceof Object[]) {
            return Long.valueOf(((Object[]) singleResult)[0].toString()).longValue();
        }
        return Long.valueOf(singleResult.toString()).longValue();
    }

    @Override
    public long getTotalCount(String hql, Object... params) {
        return this.getTotalCount(hql,
                params == null ? null : Arrays.asList(params));
    }

    protected String prepareCountHql(String hql) {

        String fromHql = hql;
        fromHql = " from " + StringUtils.substringAfter(fromHql, "from ");
        fromHql = StringUtils.substringBefore(fromHql, "order by");
//取出查询的字段
        String selectWhat = StringUtils.substringBetween(hql, "select", "from");
//        如果是new ClassName (x.x.x)格式的处理
        if (selectWhat != null && selectWhat.contains("new ") && selectWhat.contains("(") && selectWhat.contains(")")) {
            selectWhat = StringUtils.substringBetween(selectWhat, "(", ")");
        }
//        第一列查询总行数
        String countHql = "select count(*)" + (selectWhat == null ? "" : ", " + selectWhat + " ") + fromHql;
        return countHql;

    }

    protected String getCountSql(String originalHql,
                                 SessionFactory sessionFactory) {

        SessionFactoryImplementor sessionFactoryImplementor = (SessionFactoryImplementor) sessionFactory;

        HQLQueryPlan hqlQueryPlan = sessionFactoryImplementor
                .getQueryPlanCache().getHQLQueryPlan(originalHql, false, Collections.EMPTY_MAP);

        String[] sqls = hqlQueryPlan.getSqlStrings();

        String countSql = "select count(*) from (" + sqls[0] + ") count";

        return countSql;

    }

    public SessionFactoryImplementor getSessionFactoryImplementor() {

        return (SessionFactoryImplementor) sessionFactory;

    }

    public QueryPlanCache getQueryPlanCache() {

        return getSessionFactoryImplementor().getQueryPlanCache();

    }

    public HQLQueryPlan getHqlQueryPlan(String hql) {

        return getQueryPlanCache().getHQLQueryPlan(hql, false,
                Collections.EMPTY_MAP);

    }

    protected String prepareCountSql(String sql) {
        return getCountSql(sql, sessionFactory);

    }

    @Override
    public <T> List<T> findList(Class<T> clazz, String hql, List<Object> params) {
        Query q = getCurrentSession().createQuery(hql);
        if (params != null) {
            for (int i = 0; i < params.size(); i++) {
                q.setParameter(i, params.get(i));
            }
        }
        return q.list();
    }

    @Override
    public <T> List<T> findListLimit(Class<T> clazz, String hql, int limit,
                                     List<Object> params) {
        Query q = getCurrentSession().createQuery(hql);
        if (params != null) {
            for (int i = 0; i < params.size(); i++) {
                q.setParameter(i, params.get(i));
            }
        }
        q.setFirstResult(0);
        q.setMaxResults(limit);
        return q.list();
    }

    @Override
    public T getUniqueResult(String hql, Object... params) {
        return getUniqueResult(this.entityClass, hql, params);
    }

    @Override
    public <T> T getUniqueResult(Class<T> clazz, String hql, List<Object> params) {
        Query q = getCurrentSession().createQuery(hql);
        if (params != null) {
            for (int i = 0; i < params.size(); i++) {
                q.setParameter(i, params.get(i));
            }
        }
        q.setFirstResult(0);
        q.setMaxResults(1);
        List list = q.list();
        if (list == null || list.isEmpty() || list.get(0) == null) {
            return null;
        }
        return (T) list.get(0);
    }

    @Override
    public <T> List<T> findList(Class<T> clazz, String hql, Object... params) {
        return this.findList(clazz, hql,
                params == null ? null : Arrays.asList(params));
    }

    @Override
    /**
     * 用给定的HQL和参数查询前几条给定类型的数据列表
     * @param clazz
     * @param hql
     * @param limit
     * @param params
     * @return
     */
    public <T> List<T> findListLimit(Class<T> clazz, String hql, int limit,
                                     Object... params) {
        return this.findListLimit(clazz, hql, limit, params == null ? null
                : Arrays.asList(params));
    }

    @Override
    public <T> List<T> findPageList(int page, int pageSize, Class<T> clazz, String hql,
                                    Object... params) {
        if (params != null && params.length == 1 && params[0] instanceof List) {
            return findPageList(page, pageSize, clazz, hql, params[0]);
        }
        return findPageList(page, pageSize, clazz, hql, Arrays.asList(params));
    }

    @Override
    public <T> List<T> findPageList(int page, int pageSize, Class<T> clazz, String hql,
                                    List<Object> params) {
        Query q = getCurrentSession().createQuery(hql);
        q.setFirstResult((page - 1) * pageSize)
                .setMaxResults(pageSize);
        if (params != null) {
            for (int i = 0; i < params.size(); i++) {
                q.setParameter(i, params.get(i));
            }
        }
        return q.list();

    }

    @Override
    public T getUniqueResultByProperty(String propertyName, Object value) {
        return this.getUniqueResult("from " + this.entityClass.getName()
                + " where " + propertyName + "=?", value);
    }

    @Override
    public <T> T getUniqueResult(Class<T> clazz, String hql, Object... params) {
        return this.getUniqueResult(clazz, hql,
                params == null ? null : Arrays.asList(params));
    }

    @Override
    public void execute(String hql, List<Object> params) {
        Query q = this.getCurrentSession().createQuery(hql);
        if (params != null) {
            for (int i = 0; i < params.size(); i++) {
                q.setParameter(i, params.get(i));
            }
        }
        q.executeUpdate();
    }

    @Override
    public void execute(String hql, Object... params) {
        this.execute(hql, params == null ? null : Arrays.asList(params));
    }

    @Override
    public void updateProperty(PK id, String propertyName, Object status) {
        String hql = "update " + this.entityClass.getName() + " set " + propertyName + " = ? where " + sessionFactory.getClassMetadata(this.entityClass).getIdentifierPropertyName() + "= ?";
        this.execute(hql, status, id);
    }

    @Override
    public void incr(PK id, String propertyName) {
        String hql = "update " + this.entityClass.getName() + " set " + propertyName + " = " + propertyName + "+1 where " + sessionFactory.getClassMetadata(this.entityClass).getIdentifierPropertyName() + "= ?";
        this.execute(hql, id);
    }

    @Override
    public void incr(PK id, String propertyName, Integer n) {
        String hql = "update " + this.entityClass.getName() + " set " + propertyName + " = " + propertyName + "+1 where " + sessionFactory.getClassMetadata(this.entityClass).getIdentifierPropertyName() + "= ?";
        this.execute(hql, id, n);
    }

    @Override
    public void updateProperties(PK id, String[] propertyNames, Object[] values) {
        String hql = "update " + entityClass.getName() + " set ";
        for (int i = 0; i < propertyNames.length; i++) {
            hql += propertyNames[i] + "= :v" + i;
            if (i + 1 < propertyNames.length) {
                hql += ",";
            }
        }
        hql += " where " + getIdPropertyName() + " =:id";
        Query query = getCurrentSession().createQuery(hql);
        for (int i = 0; i < values.length; i++) {
            query.setParameter("v" + i, values[i]);
        }
        query.setParameter("id", id).executeUpdate();
    }

    public String getIdPropertyName() {
        return sessionFactory.getClassMetadata(this.entityClass).getIdentifierPropertyName();
    }

    @Override
    public boolean deleteAll() {
        String hql = "delete " + this.entityClass.getName();
        this.execute(hql);
        return true;
    }
}
